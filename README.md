# OpenClassroom project 4
# Chess Tournament Manager
This project is a student project designed to assist the local chess club in organizing and managing offline chess tournaments. This program follows the Model-View-Controller (MVC) design pattern to ensure a clear and maintainable structure.
## Overview
The primary goal of this project is to create a chess tournament management software that works offline, with the ability to save and load data. The main entities in the system are players, tournaments, and rounds. The program uses JSON files for data persistence.
## Objectives
### Offline and User-Friendly
* The program should be executable from the console using the command `python <program name>.py`.
* Works on Windows, Mac, or Linux.
* No internet dependency.
### Model-View-Controller (MVC)
* Use the Model-View-Controller design for a clear and maintainable structure.
* Division into packages: models, views, and controllers.
### Player Management
* Addition of players with information such as last name, first name, and date of birth.
* Use of the chess national identifier.
### Tournament Management
* Recording of tournaments with details such as name, location, start and end date, number of rounds, etc.
* Storage of tournament information in JSON files.
### Tournament Flow
* Creation of rounds with matches between players.
* Awarding points to players based on match results.
### Pairing Generation
* Dynamic generation of match pairs based on results.
* Avoidance of identical matches.
### Reports
* Display of various reports, such as the list of players, list of tournaments, etc.
### Data Save/Load
* Save and load program state via JSON files.
* Synchronization of in-memory objects with JSON files.
## Key Steps
### Environment Setup
* Use pip to define the Python environment.
* Use flake8 for linting with a maximum line length of 119.
### Creation of Models, Views, and Controllers
* Development of classes for players, tournaments, rounds, views, and controllers.
### Implementation of Core Features
* Player addition, tournament creation, round management, pairing generation, point assignment, etc.
### Testing and Debugging
* Thorough testing of each feature.
* Debugging to ensure the program functions correctly.
### Documentation
* Write a detailed README.md explaining how to use the program.
* Generate a flake8-html report to ensure compliance with PEP 8 guidelines.
